<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class UserFeedback extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_feedbacks';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = [];

    public function scopeSearch(Builder $builder, string $query): Builder
    {
        return $builder->where(function (Builder $builder) use ($query) {
            $query = trim('%'.$query.'%');

            $builder->where('title', 'like', $query);
        });
    }

    /**
     * Get the user associated with the feedback.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the associated comments.
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'user_feedback_id');
    }

    /**
     * Get the associated votes.
     */
    public function votes(): HasMany
    {
        return $this->hasMany(Vote::class, 'user_feedback_id');
    }

    /**
     * Get search based vote.
     */
    public function userVote(string $key,  string $value): mixed
    {
        return $this->votes()->where($key, $value)->first();
    }
}
