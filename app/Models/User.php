<?php

namespace App\Models;

use App\Traits\MediaConversions;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasPermissions;
use Spatie\Permission\Traits\HasRoles;

/**
 * @property-read Collection $media
 */
class User extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use HasApiTokens,
        HasFactory,
        HasPermissions,
        HasRoles,
        InteractsWithMedia,
        MediaConversions,
        Notifiable {
            MediaConversions::registerMediaConversions insteadof InteractsWithMedia;
        }

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'is_admin',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = ['first_name', 'last_name'];

    public function profileImage(): Attribute
    {
        return new Attribute(
            fn ($value) => 'https://images.unsplash.com/photo-1502378735452-bc7d86632805?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=aa3a807e1bbdfd4364d1f449eaa96d82'
        );
    }

    /**
     * Apply the basic search scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $term
     * @return void
     */
    public function scopeSearch($query, $term)
    {
        if (! $term) {
            return;
        }

        $parts = explode(' ', $term);

        foreach ($parts as $part) {
            $query->where('email', 'LIKE', "%$part%")
                ->orWhere('name', 'LIKE', "%$part%");
        }
    }

    public function getFirstNameAttribute(): string
    {
        $nameParts = explode(' ', $this->attributes['name'] ?? '');

        return trim($nameParts[0]);
    }

    public function getLastNameAttribute(): string
    {
        $nameParts = explode(' ', $this->attributes['name'] ?? '', 2);

        return trim(last($nameParts));
    }

    public function setFirstNameAttribute(string $value): void
    {
        $this->attributes['name'] = trim($value).' '.$this->last_name;
    }

    public function setLastNameAttribute(string $value): void
    {
        $this->attributes['name'] = $this->first_name.' '.trim($value);
    }

    /**
     * Get the associated comments.
     */
    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'user_id');
    }
}
