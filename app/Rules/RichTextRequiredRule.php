<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RichTextRequiredRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(private ?int $max = null, private ?int $min = null, private ?bool $emptyContent = true)
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $content = strip_tags($value);

        if (empty($content)) {
            return $this->emptyContent;
        }

        if ($this->min && strlen($content) < $this->min) {
            return false;
        }

        if ($this->max && strlen($content) > $this->max) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $message = 'The :attribute field';

        if ($this->min !== null && $this->max !== null) {
            $message .= ' must be between '.$this->min.' and '.$this->max.' characters.';
        } else {
            $message .= ' must not be empty.';
        }

        return $message;
    }
}
