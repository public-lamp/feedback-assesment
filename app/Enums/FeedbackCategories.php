<?php

namespace App\Enums;

enum FeedbackCategories: string
{
    case BUG = 'Bug Report';
    case FEATURE = 'Feature Request';
    case IMPROVEMENT = 'Improvement suggestion';
}
