<?php

namespace App\View\Components\Admin\Components\Input;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Ckeditor extends Component
{
    /**
     * The initial value.
     *
     * @var string
     */
    public $initialValue = null;

    /**
     * Instantiate the component.
     *
     * @param  string  $initialValue
     */
    public function __construct($initialValue = null)
    {
        $this->initialValue = $initialValue;
    }

    /**
     * Get a unique instance id.
     *
     * @return string
     */
    protected function getInstanceId()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 25; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('admin.components.input.ckeditor', [
            'instanceId' => $this->getInstanceId(),
        ]);
    }
}
