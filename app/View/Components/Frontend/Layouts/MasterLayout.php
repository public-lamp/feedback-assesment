<?php

namespace App\View\Components\Frontend\Layouts;

use Illuminate\Support\Str;
use Illuminate\View\Component;

class MasterLayout extends Component
{
    /**
     * The page title.
     */
    public string $title;

    /**
     * The page title.
     */
    public string $description;

    /**
     * The page keywords.
     */
    public string $keywords;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $title, string $description = '', string $keywords = '')
    {
        $this->title = $title;
        $this->description = $description;
        $this->keywords = $keywords;

        if (empty($this->keywords)) {
            $this->keywords = collect(explode(',', Str::lower(preg_replace(
                '/\s+/',
                ',',
                trim($this->title).' '.trim($this->description)
            ))))->unique()->implode(',');
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('frontend.layouts.master');
    }
}
