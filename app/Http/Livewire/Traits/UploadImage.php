<?php

namespace App\Http\Livewire\Traits;

use Livewire\TemporaryUploadedFile;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait UploadImage
{
    /**
     * @param  mixed  $model
     * @param  TemporaryUploadedFile|Media|string|null  $imageObject
     * @param  string|null  $collectionName
     */
    public function upload($model, $imageObject, $collectionName = null): ?Media
    {
        if (is_null($collectionName)) {
            $collectionName = $model::class;
        }

        if ($imageObject instanceof TemporaryUploadedFile) {
            $model->clearMediaCollection($collectionName);
            $model->addMedia($imageObject->getRealPath())
                ->usingName($imageObject->getClientOriginalName())
                ->toMediaCollection($collectionName);

            return $model->getFirstMedia($collectionName);
        }

        return null;
    }
}
