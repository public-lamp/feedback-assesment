<?php

namespace App\Http\Livewire\Traits;

use Livewire\TemporaryUploadedFile;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait RemoveImage
{
    public TemporaryUploadedFile|Media|string|null $image = null;

    /**
     * @param  string  $image
     */
    public function remove($image): ?string
    {
        if ($image instanceof TemporaryUploadedFile || $image instanceof Media) {
            $image->delete();
        }

        return null;
    }
}
