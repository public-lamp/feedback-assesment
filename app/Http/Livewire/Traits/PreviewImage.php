<?php

namespace App\Http\Livewire\Traits;

use Livewire\TemporaryUploadedFile;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait PreviewImage
{
    public TemporaryUploadedFile|Media|string|null $image = null;

    /**
     * @param  string  $image
     */
    public function preview($image): ?string
    {
        if (! $image) {
            return null;
        }

        $method = $image instanceof Media ? 'getFullUrl' : 'temporaryUrl';

        return $image->{$method}();
    }
}
