<?php

namespace App\Http\Livewire\Admin\Feedbacks;

use App\Enums\FeedbackCategories;
use App\Http\Livewire\Traits\Authenticated;
use App\Http\Livewire\Traits\Notifies;
use App\Models\UserFeedback;
use App\View\Components\Admin\Layouts\MasterLayout;
use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\Validation\Rules\Enum;
use Livewire\Component;

abstract class AdminFeedbackAbstract extends Component
{
    use Authenticated, Notifies;

    public UserFeedback $feedback;

    /**
     * Validation rules for blog
     *
     * @return array<mixed>
     */
    protected function rules(): array
    {
        return [
            'feedback.title' => 'bail|required|max:100',
            'feedback.category' => ['bail', 'required', new Enum(FeedbackCategories::class)],
            'feedback.description' => 'bail|required|max:900',
            'feedback.comments_enabled' => 'bail|required|boolean',
        ];
    }

    /**
     * Save the FAQ in database
     */
    public function save(): void
    {
        $this->validate();

        try {
            $this->feedback->save();

            $this->notify(__('feedbacks.actions.updated'), 'admin.feedbacks.index');
        } catch (\Exception $exception) {
            $this->notify($exception->getMessage(), level: 'error');
        }
    }

    public function render(): View
    {
        return $this->view('admin.feedbacks.save-feedback-controller')
            ->with('pageTitle', __('feedbacks.edit.title'));
    }

    /**
     * method to overwrite the view method of Live-wire-component-class
     */
    public function view(string $view, Closure $closure = null): View
    {
        return tap(view($view), $closure)
            ->layout(MasterLayout::class);
    }

    /**
     * the list of pages on which FAQ is related to
     *
     * @return array<string>
     */
    public function getFeedbackCategories(): array
    {
        return FeedbackCategories::cases();
    }
}
