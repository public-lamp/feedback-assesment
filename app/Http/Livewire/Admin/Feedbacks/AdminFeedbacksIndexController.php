<?php

namespace App\Http\Livewire\Admin\Feedbacks;

use App\Http\Livewire\Traits\Notifies;
use App\Http\Livewire\Traits\ResetsPagination;
use App\Models\UserFeedback;
use App\View\Components\Admin\Layouts\MasterLayout;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;

class AdminFeedbacksIndexController extends Component
{
    use Notifies;
    use ResetsPagination;
    use WithPagination;

    public string $search = '';

    public string $sortBy = 'created_at';

    public function getFeedbacksProperty(): LengthAwarePaginator
    {
        return UserFeedback::query()
            ->when($this->search, fn ($q) => $q->search($this->search))
            ->when($this->sortBy, function ($query) {
                $query->orderBy($this->sortBy, 'DESC');
            })
            ->with(['user', 'comments'])
            ->paginate();
    }

    public function render(): View
    {
        return view('admin.feedbacks.feedbacks-index-controller')
            ->layout(MasterLayout::class);
    }
}
