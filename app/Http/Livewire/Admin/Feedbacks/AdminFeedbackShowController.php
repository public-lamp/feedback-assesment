<?php

namespace App\Http\Livewire\Admin\Feedbacks;

use App\Http\Livewire\Traits\CanDeleteRecord;

class AdminFeedbackShowController extends AdminFeedbackAbstract
{
    use CanDeleteRecord;

    public function delete(): void
    {
        $this->feedback->delete();
        $this->notify(__('feedbacks.actions.deleted'), 'admin.feedbacks.index');
    }

    /**
     * return delete word to verify for delete
     */
    public function getCanDeleteConfirmationField(): string
    {
        return 'delete';
    }
}
