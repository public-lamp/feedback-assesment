<?php

namespace App\Http\Livewire\Admin\UserManagement;

use App\Http\Livewire\Traits\CanDeleteRecord;

class UserShowController extends UserAbstract
{
    use CanDeleteRecord;

    public function mount(): void
    {
        $this->changePassword = false;
    }

    public function delete(): void
    {
        $this->user->delete();
        $this->notify('User '. __('deleted'), 'admin.users-management.users.index');
    }

    /**
     * return delete word to verify for delete
     */
    public function getCanDeleteConfirmationField(): string
    {
        return 'delete';
    }
}
