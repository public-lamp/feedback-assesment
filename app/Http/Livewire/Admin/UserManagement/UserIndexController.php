<?php

namespace App\Http\Livewire\Admin\UserManagement;

use App\Http\Livewire\Traits\Notifies;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Pagination\LengthAwarePaginator;
use Livewire\WithPagination;

class UserIndexController extends UserManagementAbstract
{
    use Notifies;
    use WithPagination;

    public string $search = '';

    public function render(): View
    {
        return $this->view('admin.user-management.user-index-controller');
    }

    public function getUsersProperty(): LengthAwarePaginator
    {
        return User::query()
            ->when($this->search, fn ($q) => $q->search($this->search))
            ->paginate(10);
    }
}
