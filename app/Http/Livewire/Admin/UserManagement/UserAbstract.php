<?php

namespace App\Http\Livewire\Admin\UserManagement;

use App\Http\Livewire\Traits\HasImages;
use App\Http\Livewire\Traits\Notifies;
use App\Http\Livewire\Traits\RegistersDynamicListeners;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Livewire\WithFileUploads;

class UserAbstract extends UserManagementAbstract
{
    use HasImages;
    use Notifies;
    use RegistersDynamicListeners;
    use WithFileUploads;

    public User $user;

    public int $maxFileSize = 3; //file size in MBs

    public int $maxFiles = 1; //number of images allowed user

    public string $password;

    public string $password_confirmation;

    public bool $changePassword = true;

    /**
     * @return array<string,string>
     */
    protected function getListeners()
    {
        return array_merge(
            $this->getDynamicListeners(),
            $this->getHasImagesListeners()
        );
    }

    /**
     * Validation rules for users
     *
     * @return array<mixed>
     */
    protected function rules(): array
    {

        $rules = [
            'user.first_name' => 'bail|required|string|max:30',
            'user.last_name' => 'bail|required|string|max:30',
            'user.email' => [
                'bail', 'required', 'max:100', 'email:filter,rfc',
                Rule::unique('users', 'email')->ignore($this->user->id),
            ],
            'images' => 'bail|nullable',
        ];

        if ($this->changePassword) {
            $rules = [
                ...$rules,
                'password' => 'required|min:8|max:50|confirmed',
                'password_confirmation' => 'required',
            ];
        }

        return $rules;
    }

    public function updatedPasswordConfirmation(): void
    {
        $this->validateOnly('password');
    }

    public function updatedPassword(): void
    {
        $this->validateOnly('password');
    }

    public function getMediaModel(): Model
    {
        return $this->user;
    }

    /**
     * Save the user in database
     */
    public function save(): void
    {
        $this->validate();

        if ($this->changePassword) {
            $this->user->password = Hash::make($this->password);
        }

        $this->user->save();

        $this->updateImages();

        $type = $this->user->wasRecentlyCreated ? 'created' : 'updated';

        $this->notify("User {$type} successfully.", 'admin.users-management.users.index');
    }

    public function render(): View
    {
        $title = $this->user->id ? 'edit' : 'create';

        return $this->view('admin.user-management.user-form')
            ->with('userTitle', "user.{$title}.title");
    }
}
