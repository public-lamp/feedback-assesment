<?php

namespace App\Http\Livewire\Frontend\Feedbacks;

use App\Http\Livewire\Traits\Notifies;
use App\Http\Livewire\Traits\ResetsPagination;
use App\Models\Comment;
use App\Models\UserFeedback;
use App\Models\Vote;
use App\View\Components\Frontend\Layouts\MasterLayout;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;
use Livewire\Component;
use Livewire\WithPagination;
use Symfony\Component\HttpFoundation\Response;

class FeedbacksIndexController extends Component
{
    use Notifies;
    use ResetsPagination;
    use WithPagination;

    public string $search = '';

    public string $comment = '';

    public string $sortBy = 'created_at';

    public function mount(): void
    {
        $this->initializeCommentFields();
    }

    public function getFeedbacksProperty(): LengthAwarePaginator
    {
        return UserFeedback::query()
            ->when($this->search, fn ($q) => $q->search($this->search))
            ->when($this->sortBy, function ($query) {
                $query->orderBy($this->sortBy, 'DESC');
            })
            ->with(['user', 'comments', 'votes'])
            ->paginate(5);
    }

    public function submitVote(int $feedbackId): ?Response
    {
        Vote::create([
            'user_feedback_id' => $feedbackId,
            'user_id' => auth()->id(),
        ]);

        return $this->notify('Vote saved.');
    }

    public function render(): View
    {
        return view('frontend.pages.feedbacks.feedbacks-index-controller')
            ->layout(MasterLayout::class, [
                'title' => trans('feedbacks.index.title'),
                'description' => trans('feedbacks.index.title'),
                'keywords' => '',
            ]);
    }

    public function saveComment($feedbackId): mixed
    {
        $validator = Validator::make(
            ['comment' => $this->comment], [
                'comment' => 'bail|required|max:500',
            ]
        );

        if ($validator->fails()) {
            return $this->notify($validator->errors()->first(), level: 'error');
        }

        Comment::create([
            'user_feedback_id' => $feedbackId,
            'user_id' => auth()->id(),
            'comment' => $this->comment,
        ]);

        $this->initializeCommentFields();

        return $this->notify('Comment saved');
    }

    private function initializeCommentFields(): void
    {
        $this->comment = '';
    }
}
