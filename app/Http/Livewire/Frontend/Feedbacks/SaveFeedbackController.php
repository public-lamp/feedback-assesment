<?php

namespace App\Http\Livewire\Frontend\Feedbacks;

use App\Models\UserFeedback;

class SaveFeedbackController extends FeedbackAbstract
{
    public function mount(): void
    {
        $this->feedback = new UserFeedback();
    }
}
