<?php

namespace App\Http\Livewire\Frontend\Auth;

use App\Http\Livewire\Traits\Notifies;
use App\Models\User;
use App\Rules\PasswordValidator;
use App\View\Components\Frontend\Layouts\MasterLayout;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class RegisterController extends Component
{
    use Notifies;

    public User $user;

    public string $password;

    public string $confirmPassword;

    public function mount(): void
    {
        $this->initializeFields();
    }

    /**
     * Define the validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'user.name' => 'bail|required|max:100',
            'user.email' => 'bail|required|max:100|email|unique:users,email',
            'password' => [
                'bail',
                'required',
                'min:8',
                'max:30',
                new PasswordValidator(),
            ],
            'confirmPassword' => 'bail|required|same:password',
        ];
    }

    public function render(): View
    {
        return view('frontend.auth.register-controller')
            ->layout(MasterLayout::class, [
                'title' => trans('global.signup.title'),
                'description' => trans('global.signup.title'),
                'keywords' => '',
            ]);
    }

    /**
     * @return Response
     */
    public function register()
    {
        try {
            $this->validate();

            $this->user->password = bcrypt($this->password);

            $this->user->save();

            $this->notify(trans('notifications.account_created'), 'feedbacks.index');

            Auth::login($this->user);

            return $this->notify('Account registered successfully', 'feedbacks.index');
        } catch (\Exception $exception) {
            return $this->notify($exception->getMessage(), level: 'error');
        }
    }

    public function initializeFields(): void
    {
        $this->user = new User();
        $this->password = '';
        $this->confirmPassword = '';
    }
}
