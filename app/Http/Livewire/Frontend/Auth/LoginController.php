<?php

namespace App\Http\Livewire\Frontend\Auth;

use App\Http\Livewire\Traits\Notifies;
use App\View\Components\Frontend\Layouts\MasterLayout;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends Component
{
    use Notifies;

    public string $email;

    public string $password;

    public function mount(): void
    {
        $this->resetFields();
    }

    /**
     * Define the validation rules.
     *
     * @return array<mixed>
     */
    protected function rules()
    {
        return [
            'email' => 'bail|required|email',
            'password' => 'bail|required',
        ];
    }

    public function render(): View
    {

        return view('frontend.auth.login-controller')
            ->layout(MasterLayout::class, [
                'title' => trans('global.login.title'),
                'description' => trans('global.login.title'),
                'keywords' => '',
            ]);
    }

    /**
     * @return Response
     */
    public function login()
    {
        try {
            $this->validate();

            if (! Auth::guard('web')->attempt(['email' => $this->email, 'password' => $this->password])) {
                $this->addError('email', trans('global.invalid_credentials'));

                return $this->notify(trans('global.invalid_credentials'), level: 'error');
            }

            return $this->notify('Login successful', 'feedbacks.index');
        } catch (\Exception $exception) {
            return $this->notify($exception->getMessage(), level: 'error');
        }
    }

    public function resetFields(): void
    {
        $this->email = '';
        $this->password = '';
    }
}
