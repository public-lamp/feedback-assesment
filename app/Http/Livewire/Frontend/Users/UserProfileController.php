<?php

namespace App\Http\Livewire\Frontend\Users;

use App\View\Components\Frontend\Layouts\MasterLayout;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class UserProfileController extends Component
{
    public function render(): View
    {
        return view('frontend.users.user-profile-controller')
            ->layout(MasterLayout::class, [
                'title' => '',
                'description' => '',
                'keywords' => '',
            ]);
    }
}
