<?php

namespace App\Services\Admin;

use App\Menu\Menu;
use App\Menu\MenuItem;
use Illuminate\Support\Collection;

class MenuService
{
    public static function menu(string $name): Menu
    {
        return (new self)->get($name);
    }

    public function get(string $name): Menu
    {
        /** @var \App\Menu\Menu */
        $menu = $this->getMenuList()->where('name', $name)->first();

        return $menu;
    }

    private function getMenuList(): Collection
    {
        return collect([
            /**
             * Main Sidebar Menu
             */
            Menu::make('main')
                ->addItem(function (MenuItem $item) {
                    $item->name('Dashboard')
                        ->handle('admin.dashboard')
                        // ->permission('admin:dashboard')
                        ->icon('chart-square-bar')
                        ->route('admin.dashboard');
                })
                ->addItem(function (MenuItem $item) {
                    $item->name('User Management')
                        ->handle('admin.users-management')
                        ->permission('users-management')
                        ->icon('users')
                        ->route('admin.users-management.users.index');
                })
                ->addItem(function (MenuItem $item) {
                    $item->name('Feedbacks')
                        ->handle('admin.feedbacks')
                        ->permission('feedback')
                        ->icon('clipboard')
                        ->route('admin.feedbacks.index');
                })
                ->addItem(function (MenuItem $item) {
                    $item->name('System')
                        ->handle('admin.system')
                        ->permission('system')
                        ->icon('cog')
                        ->route('admin.system.settings');
                }),

            /**
             * System Menu
             */
            Menu::make('system')
                ->addItem(function (MenuItem $item) {
                    $item->name('Settings')
                        ->handle('admin.system.settings')
                        ->route('admin.system.settings')
                        ->permission('system:settings')
                        ->icon('cog');
                }),

            /**
             * User Management Menu
             */
            Menu::make('users-management')
                ->addItem(function (MenuItem $item) {
                    $item->name('users')
                        ->handle('admin.users-management')
                        ->route('admin.users-management.users.index')
                        ->permission('users-management:users-management')
                        ->icon('users');
                }),
        ]);
    }
}
