## Project

Feedback Assesment

## Introduction

The User Feedback Interaction is a complete web-based, Product Feedback Tool that allows users to submit, view, and vote on product feedback. The tool allows and facilitate communication between users and the product development team.

## Features

1. Admin Authentication system.
2. Admin Authorization
   Admin can perform following tasks:
    1. Login to dashboard
    2. View number of registered users
    3. Browse users list to see details
    4. Edit a user detail besides the private info
    5. Delete a User
    6. Browse Feedbacks list given by users
    7. View Feedback details and search feedbacks by title and apply different sort filters.
    8. Can active/In-active comments status on a feedback item
    9. Can Delete  a feedback item from the edit screen
    10. Settings update that is crucial for website
    11. view Admin profile and update relavent information
    12. Logout from Account

   User Features or can:
    1. Login to the platform
    2. Browse the feedbacks list of different users given
    3. Vote on a feedback item
    4. Comment on a feedback item
    5. View number of votes given by different users
    6. Emoji rich comments facility
    7. Add new feedback on the system
    8. Logout

## Prerequisites

- PHP 8.2
- Composer 2.0
- Node.js and npm
- access to bash

## Installation

1. Clone the repository:

    ```bash
    git clone https://gitlab.com/public-lamp/feedback-assesment.git
    ```

## Configuration

1. Copy the `.env.example` file to `.env` and configure the database settings.

    ```bash
    cp .env.example .env
    ```

2. Generate application key:

    ```bash
    php artisan key:generate
    ```

3. Create storage link:

    ```bash
    php artisan storage:link
    ```

4. Run database migrations:

    ```bash
    php artisan migrate:fresh --seed
    ```

5. Clear cache:

    ```bash
    php artisan optimize:clear
    ```

3. Install JavaScript dependencies:

    ```bash
    npm install && npm run dev
    ```


## Usage

To login Admin panel use following credentials:

url: http://localhost:8000/admin/dashboard (without docker)
url: http://localhost:8011/admin/dashboard (if docker is setup)

email: admin@admin.com
PW: 12345678


Test User One:
Login URL: http://localhost:8011/login (Docker)
           http://localhost:8000/login (Without Docker)

email: user.two@example.com
PW: 12345678

Test User Two:
email: user.three@example.com
PW: 12345678


In case of docker usage follow these steps:

1. Copy docker folder to .docker folder and update port DB and user in .env file inside .docker folder

   ```bash
    cp -R docker .dockr
    ```

2. Open .docker bash

   ```bash
    cd .docker
    ```

3. Ready a build of project

   ```bash
    dockr compose build --no-cache
    ```

4. Give following permissions inside the project root directory bash

   ```bash
    sudo chown -R $USER:$USER ./
    ```
   ```bash
    sudo chown -R www-data:www-data bootstrap/cache
    ```
   ```bash
    sudo chown -R www-data:www-data storage
    ```

Now move .docker directory, open terminal and follow these steps:
5. Start container

   ```bash
    dockr compose up -d
    ```

5. Bash to php service

   ```bash
    dockr exec -u docker_app_user -it feedback_php_service bash
    ```
   (feedback is the project name mentioned in .docker/.env)


7. Now make sure to restart mysql container every time you update permissions.
   Could be achieved by running commands inside the terminal of .docker folder:
   ```bash
    dockr compose down
    ```
   ```bash
    dockr compose up -d
    ```
   
8. Now in the bashed php follow the earlier mentioned configurations (project start commands)

Happy Coding!

## Developer and Maintainer

Zahid Hussain (Sr. LAMP Stack Developer)

If you find any issue in running or setup of project feel free to reach out to me on "zeeahqar@gmail.com".

## License

The project framework is open-sourced and made public to welcome contributions.


