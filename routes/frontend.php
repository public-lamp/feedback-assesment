<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Livewire\Frontend\Feedbacks\FeedbacksIndexController;
use App\Http\Livewire\Frontend\Feedbacks\SaveFeedbackController;
use App\Http\Livewire\Frontend\Users\UserProfileController;
use Illuminate\Support\Facades\Route;


Route::middleware('auth')->group(function () {
    Route::get('/', FeedbacksIndexController::class)->name('index');
    Route::prefix('user')->as('user.')->group(function () {
        Route::get('profile', UserProfileController::class)->name('profile');
    });

    Route::post('logout', [AuthenticatedSessionController::class, 'destroy'])->name('logout');

    Route::prefix('feedbacks')->as('feedbacks.')->group(function () {
        Route::get('/', FeedbacksIndexController::class)->name('index');
        Route::get('/create', SaveFeedbackController::class)->name('create');
    });
});
