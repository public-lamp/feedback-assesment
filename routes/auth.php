<?php

use App\Http\Livewire\Admin\Auth\LoginController;
use App\Http\Livewire\Admin\Auth\PasswordResetController;
use App\Http\Livewire\Frontend\Auth\LoginController as AuthLoginController;
use App\Http\Livewire\Frontend\Auth\RegisterController;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->as('admin.')->group(function () {
    Route::middleware('guest:admin')->group(function () {
        Route::get('login', LoginController::class)->name('login');
        Route::get('password-reset', PasswordResetController::class)->name('password-reset');
    });
});

Route::middleware('guest')->group(function () {
    Route::get('login', AuthLoginController::class)->name('login');
    Route::get('register', RegisterController::class)->name('register');
});
