<?php

use App\Http\Livewire\Admin\Dashboard\DashboardController;
use App\Http\Livewire\Admin\Feedbacks\AdminFeedbackShowController;
use App\Http\Livewire\Admin\Feedbacks\AdminFeedbacksIndexController;
use App\Http\Livewire\Admin\Profile\StaffProfileController;
use App\Http\Livewire\Admin\System\Settings\SettingsController;
use App\Http\Livewire\Admin\UserManagement\UserIndexController;
use App\Http\Livewire\Admin\UserManagement\UserShowController;
use Illuminate\Support\Facades\Route;

Route::middleware('auth.admin:admin')->group(function () {
    Route::get('dashboard', DashboardController::class)->name('dashboard');

    Route::prefix('users-management')->middleware('permission:users-management')->as('users-management.')->group(function () {
        Route::get('users', UserIndexController::class)->name('users.index');
        Route::get('users/{user}', UserShowController::class)->name('users.show');
    });

    Route::prefix('feedbacks')->middleware('permission:feedbacks')->as('feedbacks.')->group(function () {
        Route::get('/', AdminFeedbacksIndexController::class)->name('index');
        Route::get('/{feedback}', AdminFeedbackShowController::class)->name('show');
    });

    Route::prefix('system')->middleware('permission:system')->as('system.')->group(function () {
        Route::middleware('permission:system:settings')->group(function () {
            Route::get('settings', SettingsController::class)->name('settings');
        });
    });

    Route::get('staff/account', StaffProfileController::class)->name('staff.profile');
});
