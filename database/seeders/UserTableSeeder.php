<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        //  delete seed-able users
        $SeedAbleUserIds = [2, 3];
        User::whereIn('id', $SeedAbleUserIds)->delete();

        $users = [
            [
                'id' => 2,
                'name' => 'User-Two',
                'email' => 'user.two@example.com',
                'password' => Hash::make(12345678),
            ],
            [
                'id' => 3,
                'name' => 'User-Three',
                'email' => 'user.three@example.com',
                'password' => Hash::make(12345678),
            ],
        ];

        foreach ($users as $user) {
            User::create([
                'id' => $user['id'],
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => $user['password'],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
