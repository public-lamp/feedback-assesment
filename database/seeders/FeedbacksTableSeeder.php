<?php

namespace Database\Seeders;

use App\Enums\FeedbackCategories;
use App\Models\User;
use App\Models\UserFeedback;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class FeedbacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();

        UserFeedback::truncate();

        $feedbacks = [
            [
                'user_id' => 2,
                'category' => FeedbackCategories::IMPROVEMENT->value,
                'title' => 'Improvement Request',
                'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            ],
            [
                'user_id' => 3,
                'category' => FeedbackCategories::IMPROVEMENT->value,
                'title' => 'Improvement Request',
                'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
            ],
        ];

        foreach ($feedbacks as $item) {
            UserFeedback::create([
                'user_id' => $item['user_id'],
                'category' => $item['category'],
                'title' => $item['title'],
                'description' => $item['description'],
            ]);
        }

        Schema::enableForeignKeyConstraints();
    }
}
