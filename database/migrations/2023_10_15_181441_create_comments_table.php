<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_feedback_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->text('comment');
            $table->dateTime('comment_at')->default(date('Y-m-d H:i:s'));
            $table->timestamps();

            $table->foreign('user_feedback_id')->references('id')->on('user_feedbacks')->cascadeOnDelete();
            $table->foreign('user_id')->references('id')->on('users')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
};
