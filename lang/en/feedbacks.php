<?php

return [
    'table.columns.title' => 'Title',
    'table.columns.description' => 'Description',
    'table.columns.category' => 'Category',
    'table.columns.submitted_by' => 'Submitted By',
    'table.columns.comment_enabled' => 'Comments enabled',

    'index.title' => 'User Feedbacks',
    'index.search_placeholder' => 'Search feedbacks by title',
    'index.action.edit' => 'Edit',
    'index.action.create' => 'Give Feedback',

    'create.title' => 'Give Feedback',
    'edit.title' => 'Edit Feedback',
    'show.create' => 'Give Feedback',

    'form.inputs.title' => 'Title',
    'form.inputs.description' => 'Description',
    'form.inputs.category' => 'Select category',
    'form.danger_zone.label' => 'Remove',
    'form.danger_zone.instructions' => 'Enter the `delete` keyword to confirm removal.',

    'actions.created' => 'Feedback given successfully',
    'actions.updated' => 'feedback updated successfully',
    'actions.deleted' => 'Feedback deleted successfully',

    'sort.latest' => 'Latest',
    'sort.title' => 'Title',
    'sort.category' => 'Category',
];
