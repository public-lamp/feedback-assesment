<div x-ref="editor" x-data="{
    value: @entangle($attributes->wire('model')),
    init() {
        let editor = CKEDITOR.replace($refs.editor, {
            plugins: 'mentions,emoji,basicstyles,undo,link,wysiwygarea,toolbar,pastefromgdocs,pastefromlibreoffice,pastefromword',
            height: 80,
            toolbar: [
                {
                    name: 'document',
                    items: ['Undo', 'Redo']
                },
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Strike']
                },
                {
                    name: 'links',
                    items: ['EmojiPanel', 'Link', 'Unlink']
                }
            ],
        });

        editor.on('change', () => {
            this.value = editor.getData();
            this.$dispatch('ckeditor-input', editor.getData());
        });
    }
}" x-on:ckeditor-input="value = $event.detail" wire:ignore>
    <div>
        <textarea x-ref="editor" x-model="value" x-bind:value="value">{!! $initialValue !!}</textarea>
    </div>
</div>










