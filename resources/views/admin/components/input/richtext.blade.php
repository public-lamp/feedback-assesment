
<script src="https://cdn.jsdelivr.net/npm/rich-text-editor@7.2.3-5/dist/rich-text-editor.min.js"></script>
<div x-ref="input" x-data="{
    value: @entangle($attributes->wire('model')),
    init() {
        {{ $instanceId }} = new Quill($refs.editor, {
            theme: 'snow'
        })

        {{ $instanceId }}.on('text-change', () => {
            $dispatch('quill-input', {{ $instanceId }}.root.innerHTML)
        })
    }
}" x-on:quill-input="value = $event.detail" wire:ignore>
    <div>
        <div x-ref="editor">{!! $initialValue !!}</div>
    </div>
</div>
