<div>
    <x-slot name="pageTitle">
        @lang($userTitle)
    </x-slot>
    <form action="submit" method="POST" wire:submit.prevent="save">
        <div class="grid grid-cols-12">
            <div class="col-span-12 space-y-4">
                <x-admin.components.card heading="Basic Information">
                    <div class="grid grid-cols-2 gap-4">
                        <x-admin.components.input.group for="firstname" label="{{ __('inputs.firstname') }}" :error="$errors->first('user.first_name')">
                            <x-admin.components.input.text id="firstname" name="firstname" wire:model="user.first_name" :error="$errors->first('user.first_name')" />
                        </x-admin.components.input.group>
                        <x-admin.components.input.group for="lastname" label="{{ __('inputs.lastname') }}" :error="$errors->first('user.last_name')">
                            <x-admin.components.input.text id="lastname" name="lastname" wire:model="user.last_name" :error="$errors->first('user.last_name')" />
                        </x-admin.components.input.group>
                    </div>
                    <x-admin.components.input.group for="email" label="{{ __('inputs.email') }}" :error="$errors->first('user.email')">
                        <x-admin.components.input.text id="email" name="email" type="email" :disabled="$user->id ? true : false" wire:model="user.email" :error="$errors->first('user.email')" />
                    </x-admin.components.input.group>

                    <div id="images">
                        <x-admin.components.image-manager :existing="$images" :maxFileSize="$maxFileSize" :maxFiles="$maxFiles" :multiple="false" model="imageUploadQueue" />
                    </div>
                </x-admin.components.card>

                <div class="px-4 py-3 text-right rounded shadow bg-gray-50 sm:px-6">
                    <button class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" type="submit">
                        {{ __($user->id ? 'user.index.action.update' : 'user.index.action.save') }}
                    </button>
                </div>

                @if($user->exists && !$user->wasRecentlyCreated)
                    <div class="bg-white border border-red-300 rounded shadow">
                        <header class="px-6 py-4 text-red-700 bg-white border-b border-red-300 rounded-t">
                            {{ __('inputs.danger_zone.title') }}
                        </header>
                        <div class="p-6 space-y-4 text-sm">
                            <div class="grid grid-cols-12 gap-4">
                                <div class="col-span-12 md:col-span-6">
                                    <strong>{{ __('global.danger.zone.label') }}</strong>
                                    <p class="text-xs text-gray-600">{{ __('global.danger.zone.instructions') }}</p>
                                </div>
                                <div class="col-span-9 lg:col-span-4">
                                    <x-admin.components.input.text wire:model="deleteConfirm" />
                                </div>
                                <div class="col-span-3 text-right lg:col-span-2">
                                    <x-admin.components.button theme="danger" :disabled="!$this->canDelete"
                                                               wire:click="delete" type="button">{{ __('global.delete') }}
                                    </x-admin.components.button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </form>
</div>
