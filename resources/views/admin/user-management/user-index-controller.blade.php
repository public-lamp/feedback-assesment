<x-slot name="pageTitle">
    {{ __('user.index.title') }}
</x-slot>

<div>
    <div class="border border-gray-300 shadow-gray-800 dark:shadow-gray-50 dark:border-gray-500 sm:rounded-lg">

        <div class="p-4 space-y-4">

            <div class="flex items-center space-x-4">
                <div class="grid w-full grid-cols-12 space-x-4">
                    <div class="col-span-12 md:col-span-12">
                        <x-admin.components.input.text wire:model.debounce.300ms="search" placeholder="{{ __('user.index.search.placeholder') }}" />
                    </div>
                </div>
            </div>
        </div>

        <x-admin.components.table class="w-full p-2 whitespace-no-wrap">
            <x-slot name="head">
                <x-admin.components.table.heading></x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('global.firstname') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('global.lastname') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('global.email') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('global.active') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading></x-admin.components.table.heading>
            </x-slot>
            <x-slot name="body">
                @forelse($this->users as $user)
                    <x-admin.components.table.row wire:loading.class.delay="opacity-50">
                        <x-admin.components.table.cell>
                            <img class="object-cover w-8 h-8 rounded-full" src="{{ $user->thumbnail ? $user->getThumbnailUrl('small') : $user->profileImage }}" alt="" aria-hidden="true" />
                        </x-admin.components.table.cell>
                        <x-admin.components.table.cell>{{ $user->first_name }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>{{ $user->last_name }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>
                            {{ $user->email }}
                        </x-admin.components.table.cell>
                        <x-admin.components.table.cell>
                            <x-icon style="solid" :ref="!$user->deleted_at ? 'check' : 'x'" :class="!$user->deleted_at ? 'text-green-500' : 'text-red-500'" />
                        </x-admin.components.table.cell>
                        <x-admin.components.table.cell>
                            @if (!$user->deleted_at)
                                <a class="text-indigo-500 hover:underline" href="{{ route('admin.users-management.users.show', $user->id) }}">
                                    {{ __('user.index.action.edit') }}
                                </a>
                            @endif
                        </x-admin.components.table.cell>
                    </x-admin.components.table.row>
                @empty
                    <x-admin.components.table.no-results />
                @endforelse
            </x-slot>
        </x-admin.components.table>

        @if ($this->users->hasPages())
            <div class="p-4 space-y-4">
                {{ $this->users->links() }}
            </div>
        @endif
    </div>
</div>
