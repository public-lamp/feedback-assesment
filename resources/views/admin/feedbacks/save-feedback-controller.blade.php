<div>
    <x-slot name="title">
        {{ $pageTitle }}
    </x-slot>

    <form method="POST" wire:submit.prevent="save">
        <div class="grid grid-cols-12">
            <div class="col-span-12 space-y-4">
                <x-admin.components.card heading="">
                    <header>
                        <h3 class="text-lg font-medium leading-6 text-gray-900">
                            {{ $pageTitle }}
                        </h3>
                    </header>
                    <div class="grid grid-cols-1">
                        <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.title') }}"
                                                        for="title" :error="$errors->first('feedback.title')" required="true">
                            <x-admin.components.input.text wire:model.defer="feedback.title" id="title"
                                                           :error="$errors->first('feedback.title')" readonly="" />
                        </x-admin.components.input.group>
                    </div>
                    <div class="grid grid-cols-1">
                        <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.category')  }}"
                                                        for="category" :error="$errors->first('feedback.title')" required="true">
                            <x-admin.components.input.text wire:model.defer="feedback.category" id="category"
                                                           :error="$errors->first('feedback.category')" readonly="" />
                        </x-admin.components.input.group>
                    </div>
                    <div class="grid grid-cols-1">
                        <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.description') }}"
                                                        for="description" :error="$errors->first('feedback.description')" required="true">
                            <x-admin.components.input.textarea wire:model.defer="feedback.description" id="description"
                                                               :error="$errors->first('feedback.description')" readonly="" />
                        </x-admin.components.input.group>
                    </div>
                </x-admin.components.card>

                <div class="px-4 py-3 text-right rounded shadow bg-gray-50 sm:px-6">
                    <div class="flex flex-row justify-end gap-x-2">
                        <x-admin.components.active-dropdown wire:model.defer="feedback.comments_enabled" type="feedback" />
                        <button type="submit"
                                class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.remove>
                                {{ __('global.save') }}
                            </span>
                            @include('admin.layouts.livewire.button-loading')
                        </button>
                    </div>
                </div>

                @if ($feedback->exists && !$feedback->wasRecentlyCreated)
                    <div class="bg-white border border-red-300 rounded shadow">
                        <header class="px-6 py-4 text-red-700 bg-white border-b border-red-300 rounded-t">
                            {{ __('inputs.danger_zone.title') }}
                        </header>
                        <div class="p-6 space-y-4 text-sm">
                            <div class="grid grid-cols-12 gap-4">
                                <div class="col-span-12 md:col-span-6">
                                    <strong>{{ __('feedbacks.form.danger_zone.label') }}</strong>
                                    <p class="text-xs text-gray-600">{{ __('feedbacks.form.danger_zone.instructions') }}</p>
                                </div>
                                <div class="col-span-9 lg:col-span-4">
                                    <x-admin.components.input.text wire:model="deleteConfirm" />
                                </div>
                                <div class="col-span-3 text-right lg:col-span-2">
                                    <x-admin.components.button theme="danger" :disabled="!$this->canDelete" wire:click="delete"
                                                               type="button">{{ __('global.delete') }}
                                    </x-admin.components.button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </form>
</div>
