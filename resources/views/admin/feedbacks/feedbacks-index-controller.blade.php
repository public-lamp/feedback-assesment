<x-slot name="title">
    {{ __('feedbacks.index.title') }}
</x-slot>

<div>
    <div class="shadow-gray-800 dark:shadow-gray-50 border border-gray-300 dark:border-gray-500 sm:rounded-lg">
        <div class="p-4 space-y-4">

            <div class="flex items-center space-x-4">
                <div class="grid grid-cols-12 w-full space-x-4">
                    <div class="col-span-8 md:col-span-8">
                        <x-admin.components.input.text wire:model.debounce.300ms="search" placeholder="{{ __('feedbacks.index.search_placeholder') }}" />
                    </div>
                    <div class="col-span-3 text-right md:col-span-3">
                        <div class="sort-item">
                            <select name="sortBy"
                                    class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                    wire:model="sortBy">
                                <option value="created_at" selected="selected">{{ __('feedbacks.sort.latest') }}</option>
                                <option value="title">{{ __('feedbacks.sort.title') }}</option>
                                <option value="category">{{ __('feedbacks.sort.category') }}</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <x-admin.components.table class="w-full whitespace-no-wrap p-2">
            <x-slot name="head">
                <x-admin.components.table.heading>{{ __('feedbacks.table.columns.title') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('feedbacks.table.columns.description') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('feedbacks.table.columns.category') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('feedbacks.table.columns.submitted_by') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('feedbacks.table.columns.comment_enabled') }}</x-admin.components.table.heading>
                <x-admin.components.table.heading>{{ __('global.actions') }}</x-admin.components.table.heading>
            </x-slot>
            <x-slot name="body">
                @forelse($this->feedbacks as $feedback)
                    <x-admin.components.table.row wire:loading.class.delay="opacity-50">
                        <x-admin.components.table.cell>{{ $feedback->title }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>{{ $feedback->description }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>{{ $feedback->category }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>{{ $feedback->user->name }}</x-admin.components.table.cell>
                        <x-admin.components.table.cell>
                            <x-icon :ref="$feedback->comments_enabled ? 'check' : 'x'" :class="$feedback->comments_enabled ? 'text-green-500' : 'text-red-500'" style="solid" />
                        </x-admin.components.table.cell>
                        <x-admin.components.table.cell>
                            @if ($feedback)
                                <a href="{{ route('admin.feedbacks.show', $feedback->id) }}"
                                   class="text-indigo-500 hover:underline">
                                    {{ __('feedbacks.index.action.edit') }}
                                </a>
                            @endif
                        </x-admin.components.table.cell>
                    </x-admin.components.table.row>
                @empty
                    <x-admin.components.table.no-results />
                @endforelse
            </x-slot>
        </x-admin.components.table>

        @if ($this->feedbacks->hasPages())
            <div class="p-4 space-y-4">
                {{ $this->feedbacks->links() }}
            </div>
        @endif
    </div>
</div>

