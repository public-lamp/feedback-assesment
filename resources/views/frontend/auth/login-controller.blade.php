<div>
    <section class="bg-gray-100 lg:px-12 md:px-6 sm:px-2 lg:mt-4 md:mt-2">
        <div class="h-full">
            <!-- Left column container with background-->
            <div class="g-6 flex h-full flex-wrap items-center justify-center lg:justify-between">
                <div class="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
                    <img
                        src="https://tecdn.b-cdn.net/img/Photos/new-templates/bootstrap-login-form/draw2.webp"
                        class="w-full"
                        alt="Sample image" />
                </div>

                <!-- Right column container -->
                <div class="mb-12 md:mb-0 md:w-8/12 lg:w-5/12 xl:w-5/12 border-4 p-4">
                    <form wire:submit.prevent="login">
                        <div class="relative mb-6">
                            <label for="email" class="text-sm font-semibold text-gray-600">Email</label>
                            <input type="text" id="email" wire:model.defer="email" class="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500" placeholder="Email">
                            @error('email')
                            <div class="error text-red-600">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="relative mb-6">
                            <label for="password" class="text-sm font-semibold text-gray-600">Password</label>
                            <input type="password" id="email" wire:model.defer="password" class="w-full px-4 py-2 border rounded-md focus:outline-none focus:border-blue-500" placeholder="Password">
                            @error('password')
                            <div class="error text-red-600">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>

                        <div class="text-center lg:text-left">
                            <div class="grid-cols-1 mb-2">
                                <div class="flex flex-row justify-center gap-x-2">
                                    <button type="submit"
                                            class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                    <span wire:loading.remove>
                                        Login
                                    </span>
                                        @include('admin.layouts.livewire.button-loading')
                                    </button>
                                </div>
                            </div>

                            <!-- Register link -->
                            <p class="mb-0 mt-2 pt-1 text-sm font-semibold">
                                Don't have an account?
                                <a href="{{ route('register')}}" class="text-danger transition duration-150 ease-in-out hover:text-danger-600 focus:text-danger-600 active:text-danger-700">
                                    Register
                                </a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
