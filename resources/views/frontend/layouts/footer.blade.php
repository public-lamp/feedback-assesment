<div>
    <footer class="bg-gray-600 text-white p-3">
        <div class="flex justify-center items-center space-x-4 text-center">
            @if (config('custom.facebook_url'))
                <a href="{{ config('custom.facebook_url') }}" target="_blank" class="social-link" style="color: #1877f2; font-size: 25px; width: 25px; height: 25px;"><i class="fa-brands fa-facebook"></i></a>
            @endif
            @if (config('custom.twitter_url'))
                <a href="{{ config('custom.twitter_url') }}" target="_blank" class="social-link" style="color: #1da1f2; font-size: 25px; width: 25px; height: 25px;"><i class="fa-brands fa-x-twitter"></i></a>
            @endif
            @if (config('custom.instagram_url'))
                <a href="{{ config('custom.instagram_url') }}" target="_blank" class="social-link" style="color: #e4405f; font-size: 25px; width: 25px; height: 25px;"><i class="fa-brands fa-instagram"></i></a>
            @endif
            <p class="pt-2">&copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.</p>
        </div>
    </footer>
</div>
