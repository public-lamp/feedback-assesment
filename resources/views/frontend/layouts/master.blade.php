<!DOCTYPE html>
<html lang="en">

<head>
    <title> @yield('meta_title', $title ?? '') | {{ config('app.name') }} </title>
    <link rel="icon" sizes="57x57" href="/frontend/images/sound.svg">

    <meta charset="UTF-8">
    <meta name="author" content="Box Store">
    <meta name="robots" content="index, follow">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=3.0">

    <meta name="description" content="@yield('description', $description ?? '')">
    <meta name="keywords" content="@yield('meta_keywords', $keywords ?? '')">
    <meta property="og:title" content="@yield('meta_title')">
    <meta property="og:description" content=@yield('meta_description')>
    <meta property="og:image" content="@yield('image', '/frontend/images/sound.svg')">
    <meta property="og:url" content="@yield('url', request()->url())">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="@yield('title')">
    <meta name="twitter:description" content="@yield('description')">
    <meta name="twitter:image" content="@yield('image', '/images/sound.svg')">

    @include('frontend.layouts.css')
    @stack('page_css')
    @livewireStyles
</head>

<body style="box-sizing:border-box; overflow-x:hidden;">
<div class="flex flex-col min-h-screen">
    <div class="container main-site flex-grow flex-wrap">
        @include('frontend.layouts.header')

        <div class="clearfix"></div>

        <div class="pt-16"></div>

        <div class="flex items-center justify-center pt-4">
            <strong class="text-lg font-bold md:text-2xl text-black dark:text-white">
                @yield('page_title', $title ?? '')
            </strong>

            {{ $actionButton ?? '' }}
        </div>

        <div class="w-full min-h-screen">
            @yield('page')
            {{ $slot ?? '' }}
        </div>

        <div class="clearfix"></div>

        <x-notify-component />
    </div>

    <div class="left-0 right-0 bottom-0 mt-6">
        @include('frontend.layouts.footer')
    </div>
</div>

@include('frontend.layouts.js')

@livewireScripts

@include('frontend.layouts.livewirejs')

{{--@include('frontend.layouts.toastr-events')--}}

@stack('page_js')

</body>

</html>
