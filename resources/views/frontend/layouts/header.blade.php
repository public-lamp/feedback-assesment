<div class="header-wrap shadow fixed mb-8 right-0 left-0 top-0 z-10">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light">
            <div class="w-100">

                <div class="sm:flex justify-between">
                    <div class="d-md-flex justify-content-start align-items-start d-none my-1">
                        <a class="navbar-brand" href="/">
                            <img src="https://www.ikonlink.com/wp-content/uploads/2020/06/ikon-website-logo-200.png"
                                 class="img-fluid" alt="Site Logo"
                                 style="max-height: 60px; max-width: 60px;"
                            >
                        </a>
                    </div>
                    <div class="my-1 mx-12 lg:mx-0">
                        <a class="primary-button bg-green-400 text-black md:px-2 md:py-1 rounded-md hover:bg-green-500 focus:outline-none focus:shadow-outline-red active:bg-green-600" href="{{ route('feedbacks.index') }}">Users Feedbacks</a>
                    </div>
                    <div class="my-1 mx-12 lg:mx-0">
                        <a class="primary-button bg-green-400 text-black md:px-2 md:py-1 rounded-md hover:bg-green-500 focus:outline-none focus:shadow-outline-red active:bg-green-600" href="{{ route('feedbacks.create') }}">Give Feedbacks</a>
                    </div>
                    <div class="justify-content-end my-1 mx-12 lg:mx-0">
                        @auth
                            <a class="primary-button bg-red-500 text-white px-4 py-2 rounded-md hover:bg-red-600 focus:outline-none focus:shadow-outline-red active:bg-red-800" href="javascript:void(0);" onclick="event.preventDefault(); document.getElementById('logoutForm').submit();">Logout</a>
                            <form id="logoutForm" action="{{ route('logout') }}" method="post">
                                @csrf
                            </form>
                        @else
                            <a href="{{ route('login') }}" class="primary-button bg-green-500 text-white px-4 py-2 rounded-md hover:bg-green-600 focus:outline-none focus:shadow-outline-green active:bg-green-800 m-0">Sign In</a>
                        @endauth
                    </div>
                </div>

            </div>
        </nav>
    </div>
</div>
