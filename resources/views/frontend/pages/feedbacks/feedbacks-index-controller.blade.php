<div>
    <x-slot name="pageTitle">
        {{ __('feedbacks.index.title') }}
    </x-slot>

    <div class="bg-white p-6 mx-auto border-4 rounded mt-4">
        <!-- Feedback card -->
        @forelse($this->feedbacks as $feedbackKey => $feedback)
            <div class="bg-slate-50 mx-auto shadow my-6 p-4 w-full md:w-4/5 lg:w-3/4" wire:key="{{ $feedbackKey }}">
                <div class="flex flex-col md:flex-row items-center justify-between">
                    <strong class="text-lg font-bold md:text-2xl text-black dark:text-white mb-4 md:mb-0">
                        <h2 class="text-lg font-bold max-w-full md:max-w-md">{{ $feedback->title }}</h2>
                    </strong>
                    @if(! $feedback->userVote('user_id', auth()->id()))
                        <button type="button" wire:click="submitVote('{{ $feedback->id }}')"
                                class="inline-flex justify-center px-4 py-2 text-sm md:text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.remove>Vote</span>
                            @include('admin.layouts.livewire.button-loading')
                        </button>
                    @else
                        <span class="text-green-800 mt-4 md:mt-0 md:ml-4">Voted</span>
                    @endif
                </div>

                <div class="mb-2">
                    <span class="text-gray-800 mr-2 font-semibold">{{ $feedback->user->name }}</span>
                    <span class="text-gray-600 text-sm">{{ $feedback->created_at->format('d M, Y') }}</span>
                </div>

                <div class="mb-2">
                    <p class="text-gray-800 leading-relaxed mb-1">{{ $feedback->description }}</p>
                    <p class="text-gray-800 leading-relaxed font-bold">Category: {{ $feedback->category }}</p>
                </div>

                <a href="javascript:void(0);"><span class="cursor-pointer text-blue-500">Votes ({{ count($feedback->votes) }})</span></a>

                @if($feedback->comments_enabled)
                    <div class="mt-4">
                        <h3 class="font-medium text-lg md:text-xl">Add Your Comment</h3>
                        <div class="grid grid-cols-1 gap-y-2 md:gap-x-4">
                            <div class="mb-2">
                                <div>
                                     <x-admin.components.input.ckeditor wire:model.defer="comment" />
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="flex justify-start gap-x-2">
                                    <button type="button" wire:click="saveComment('{{ $feedback->id }}')" class="inline-flex justify-center px-4 py-2 text-sm md:text-base font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                        <span wire:loading.remove>
                                            Save Comment
                                        </span>
                                        @include('admin.layouts.livewire.button-loading')
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <h3>Comments disabled by Admin.</h3>
                @endif

                <!-- Expandable/Collapsible Comments Section -->
                <details class="mb-4">
                    <summary class="cursor-pointer text-blue-500">View All Comments ({{ count($feedback->comments) }})</summary>
                    <div class="mt-2 bg-gray-100 p-4 rounded-lg">
                        @forelse($feedback->comments as $commentKey => $comment)
                            <div class="flex items-center space-x-4 my-1" wire:key="{{ $commentKey }}">
                                <div class="w-10 h-10 rounded-full flex items-center justify-center">
                                    <img class="rounded-full" src="{{ $comment->user->thumbnail ? $comment->user->getThumbnailUrl('small') : $comment->user->profileImage }}">
                                </div>
                                <div>
                                    <p class="text-gray-800">{!! $comment->comment !!}</p>
                                    <p class="text-gray-600 text-sm">{{ $comment->user->name }} - {{ $comment->created_at->format('d D, Y') }}</p>
                                </div>
                            </div>
                        @empty
                            <p>No comments to show.</p>
                        @endforelse
                    </div>
                </details>
            </div>
        @empty
            <p>No Feedback to show.</p>
        @endforelse

        @if ($this->feedbacks->hasPages())
            <div class="p-4 space-y-4">
                {{ $this->feedbacks->links() }}
            </div>
        @endif
    </div>
</div>
