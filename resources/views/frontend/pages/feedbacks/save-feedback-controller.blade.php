<div>
    <div class="mx-auto my-6 p-4 w-full md:w-4/5 lg:w-3/4">
        <x-slot name="title">
            {{ $pageTitle }}
        </x-slot>

        <form method="POST" wire:submit.prevent="save">
            <div class="grid grid-cols-12">
                <div class="col-span-12 space-y-4">
                    <x-admin.components.card heading="">
                        <header>
                            <h3 class="text-lg font-medium leading-6 text-gray-900">
                                {{ $pageTitle }}
                            </h3>
                        </header>
                        <div class="grid grid-cols-1">
                            <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.title') }}"
                                                            for="title" :error="$errors->first('feedback.title')" required="true" >
                                <x-admin.components.input.text wire:model.defer="feedback.title" id="title"
                                                               :error="$errors->first('feedback.title')" />
                            </x-admin.components.input.group>
                        </div>
                        <div class="grid grid-cols-1">
                            <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.category') }}"
                                                            for="category" :error="$errors->first('feedback.category')" required="true" >
                                <x-admin.components.input.select wire:model.defer="feedback.category" :error="$errors->first('feedback.category')" id="category" >
                                    <option value="" selected>{{ __('feedbacks.form.inputs.category') }}</option>
                                    @foreach($this->getFeedbackCategories() as $item)
                                        <option value="{{ $item }}">{{ $item }}</option>
                                    @endforeach
                                </x-admin.components.input.select>
                            </x-admin.components.input.group>
                        </div>
                        <div class="grid grid-cols-1">
                            <x-admin.components.input.group label="{{ __('feedbacks.form.inputs.description') }}"
                                                            for="description" :error="$errors->first('feedback.description')" required="true">
                                <x-admin.components.input.textarea wire:model.defer="feedback.description" id="description"
                                                                   :error="$errors->first('feedback.description')" />
                            </x-admin.components.input.group>
                        </div>
                    </x-admin.components.card>

                    <div class="px-4 py-3 text-right rounded shadow bg-gray-50 sm:px-6">
                        <div class="flex flex-row justify-end gap-x-2">
                            <button type="submit"
                                    class="inline-flex justify-center px-4 py-2 text-sm font-medium text-white bg-indigo-600 border border-transparent rounded-md shadow-sm hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                            <span wire:loading.remove>
                                {{ __('global.save') }}
                            </span>
                                @include('admin.layouts.livewire.button-loading')
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
